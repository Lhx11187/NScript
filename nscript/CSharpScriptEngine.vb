﻿Imports System.CodeDom.Compiler

Public MustInherit Class CSharpScriptEngine
    Inherits ScriptEngine

    Public Overrides Sub ShowMainMethodNotFoundError()
        ShowError("public static int/void Main() is not found!")
    End Sub

    Protected Overrides Function GetProvider() As CodeDomProvider
        Return CodeDomProvider.CreateProvider("CSharp")
    End Function

    Protected Overrides Sub SetCompilerParameters(objCompilerParameters As CompilerParameters)
        objCompilerParameters.ReferencedAssemblies.Add("microsoft.csharp.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.core.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.data.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.data.datasetextensions.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.deployment.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.drawing.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.windows.forms.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.xml.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.xml.linq.dll")

        objCompilerParameters.CompilerOptions = "/optimize /warn:0 /nologo"
    End Sub
End Class
