﻿Imports System.CodeDom.Compiler

Public MustInherit Class VBScriptEngine
    Inherits ScriptEngine

    Public Overrides Sub ShowMainMethodNotFoundError()
        ShowError("Public [Shared] Function/Sub Main() is not found!" & vbCrLf &
                  "(keyword Shared is for Class only, not for Module.)")
    End Sub

    Protected Overrides Function GetProvider() As CodeDomProvider
        Return CodeDomProvider.CreateProvider("VisualBasic")
    End Function

    Protected Overrides Sub SetCompilerParameters(objCompilerParameters As CompilerParameters)
        objCompilerParameters.ReferencedAssemblies.Add("system.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.data.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.deployment.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.drawing.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.windows.forms.dll")
        objCompilerParameters.ReferencedAssemblies.Add("system.xml.dll")

        objCompilerParameters.CompilerOptions = "/optimize /nowarn /nologo /imports:microsoft.visualbasic,system /optionexplicit- /optionstrict- /optioninfer-"
    End Sub
End Class
