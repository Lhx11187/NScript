Set shell = createobject("wscript.shell")
Set sysenv = shell.environment("System")

addtopathext ";.LNK"
addtopathext ";.VBX"
addtopathext ";.CSX"

Sub AddToPathExt(extname)
	If InStr(LCase(sysenv("PATHEXT")),LCase(extname)) = 0 Then
		sysenv("PATHEXT") = sysenv("PATHEXT") & extname
	End If
End Sub