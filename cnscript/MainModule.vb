﻿Imports nscript

Module MainModule
    Public Function Main(args() As String) As Integer
        If args.Length = 0 Then
            Return 0
        End If

        Dim objScriptEngine As ScriptEngine = ScriptEngine.CreateScriptEngine(args(0), True)
        If objScriptEngine Is Nothing Then
            Console.WriteLine("Cannot recognize extension name: " & vbCrLf & args(0))
            Console.ReadKey()
            Return 1
        End If

        Dim intRet As Integer = objScriptEngine.Run(args)
#If DEBUG Then
        Console.ReadKey(False)
#End If
        Return intRet
    End Function
End Module