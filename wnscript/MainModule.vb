﻿Imports nscript

Module MainModule

    Public Function Main(args() As String) As Integer
        If args.Length = 0 Then
            Return 0
        End If

        Dim objScriptEngine As ScriptEngine = ScriptEngine.CreateScriptEngine(args(0), False)
        If objScriptEngine Is Nothing Then
            MsgBox("Cannot recognize extension name: " & vbCrLf & args(0), vbCritical)
            Return 1
        End If
        Return objScriptEngine.Run(args)
    End Function
End Module